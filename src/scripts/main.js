// logic picoyplaca app
/* can't use import - problem in chrome browser Uncaught ReferenceError: exports is not defined (maybe I need to use a tool to bundle
the modules) */
/* Class for PicoyPlaca App */
var PicoyPlaca = /** @class */ (function () {
    function PicoyPlaca(plateNumber, date, time) {
        this.plateNumber = plateNumber;
        this.date = date;
        this.time = time;
    }
    //validate a Plate Number
    PicoyPlaca.prototype.validatePlate = function () {
        var canDrive = true;
        var lastNumberPlate = this.plateNumber.toString().charAt(this.plateNumber.toString().length - 1);
        var validateDateToDrive = this.getValidateDateToDrive(parseInt(lastNumberPlate), this.date);
        var validateTimeToDrive = this.getValidateTimeToDrive(this.time);
        if (!validateDateToDrive && !validateTimeToDrive) {
            canDrive = false;
        }
        return canDrive;
    };
    PicoyPlaca.prototype.getValidateDateToDrive = function (lastNumber, date) {
        var dayNoDrive = { dayName: '', dayNumber: -1 };
        if (lastNumber == 1 || lastNumber == 2) {
            dayNoDrive.dayName = 'Monday';
            dayNoDrive.dayNumber = 1;
        }
        if (lastNumber == 3 || lastNumber == 4) {
            dayNoDrive.dayName = 'Tuesday';
            dayNoDrive.dayNumber = 2;
        }
        if (lastNumber == 5 || lastNumber == 6) {
            dayNoDrive.dayName = 'Wednesday';
            dayNoDrive.dayNumber = 3;
        }
        if (lastNumber == 7 || lastNumber == 8) {
            dayNoDrive.dayName = 'Thrusday';
            dayNoDrive.dayNumber = 4;
        }
        if (lastNumber == 9 || lastNumber == 0) {
            dayNoDrive.dayName = 'Friday';
            dayNoDrive.dayNumber = 5;
        }
        var dayInputDateNumber = new Date(date).getDay() + 1;
        if (dayNoDrive.dayNumber == dayInputDateNumber) {
            return false;
        }
        return true;
    };
    PicoyPlaca.prototype.getValidateTimeToDrive = function (time) {
        var time1 = '07:00';
        var time2 = '09:30';
        var time3 = '16:00';
        var time4 = '19:30';
        if ((this.getTime(time) >= this.getTime(time1) && this.getTime(time) <= this.getTime(time2)) ||
            (this.getTime(time) >= this.getTime(time3) && this.getTime(time) <= this.getTime(time4))) {
            return false;
        }
        return true;
    };
    PicoyPlaca.prototype.getTime = function (time) {
        return new Date(2019, 9, 2, time.substring(0, 2), time.substring(3, 5), 0, 0);
    };
    return PicoyPlaca;
}());
var submitBtn = document.getElementById('submitbtn');
var plateInputValue = document.getElementById('platenumber');
var dateInputValue = document.getElementById('date');
var timeInputValue = document.getElementById('time');
var retryLink = document.getElementById('retryLink');
var mainFunction = function () {
    // hide all elements when clicked again
    showHide(false, '.positive-result');
    showHide(false, '.negative-result');
    if (!validateInputs()) {
        alert('Please all fields are required...');
        return;
    }
    validatePicoyPlaca(parseInt(plateInputValue.value), dateInputValue.value, timeInputValue.value);
};
submitBtn.addEventListener('click', mainFunction);
retryLink.addEventListener('click', function () { return window.location.reload(true); });
// function to validate plateNumber
var validatePicoyPlaca = function (plateNumber, date, time) {
    var picoyPlacaObj = new PicoyPlaca(plateNumber, date, time);
    var canDrive = picoyPlacaObj.validatePlate();
    if (canDrive) {
        showHide(true, '.positive-result');
    }
    else {
        showHide(true, '.negative-result');
    }
};
var showHide = function (show, elementToHideShow) {
    var resultsContent = document.querySelector('.container__response__results');
    resultsContent.style.display = show ? 'flex' : 'none';
    var positiveResult = document.querySelector(elementToHideShow);
    positiveResult.style.display = show ? 'flex' : 'none';
};
var validateInputs = function () {
    if (plateInputValue.value !== '' && dateInputValue.value !== '' && timeInputValue.value !== '') {
        return true;
    }
    return false;
};
