// logic picoyplaca app
/* can't use import - problem in chrome browser Uncaught ReferenceError: exports is not defined (maybe I need to use a tool to bundle 
the modules) */

interface dateDay {
    dayName: string,
    dayNumber: number
}
/* Class for PicoyPlaca App */
class PicoyPlaca {
    plateNumber: number;
    date: string;
    time: string;

    constructor(plateNumber: number, date: string, time:string) {
        this.plateNumber = plateNumber;
        this.date = date;
        this.time = time;
    }
    //validate a Plate Number
    validatePlate(): boolean {
        let canDrive = true;
        let lastNumberPlate = this.plateNumber.toString().charAt(this.plateNumber.toString().length - 1);
        let validateDateToDrive = this.getValidateDateToDrive(parseInt(lastNumberPlate), this.date);
        let validateTimeToDrive = this.getValidateTimeToDrive(this.time);
        if(!validateDateToDrive && !validateTimeToDrive) {
            canDrive = false;
        }
        return canDrive;
    }

    private getValidateDateToDrive(lastNumber: number, date: string) {
        let dayNoDrive: dateDay = { dayName: '', dayNumber: -1 };
        if(lastNumber==1 || lastNumber==2) {
            dayNoDrive.dayName = 'Monday';
            dayNoDrive.dayNumber = 1;
        }
        if(lastNumber==3 || lastNumber==4) {
            dayNoDrive.dayName = 'Tuesday';
            dayNoDrive.dayNumber = 2;
        }
        if(lastNumber==5 || lastNumber==6) {
            dayNoDrive.dayName = 'Wednesday';
            dayNoDrive.dayNumber = 3;
        }
        if(lastNumber==7 || lastNumber==8) {
            dayNoDrive.dayName = 'Thrusday';
            dayNoDrive.dayNumber = 4;
        }
        if(lastNumber==9 || lastNumber==0) {
            dayNoDrive.dayName = 'Friday';
            dayNoDrive.dayNumber = 5;
        }
        let dayInputDateNumber = new Date(date).getDay() + 1;
        if(dayNoDrive.dayNumber == dayInputDateNumber) {
            return false;
        } 
        return true;
    }

    private getValidateTimeToDrive(time: string) {
        const time1 = '07:00';
        const time2 = '09:30';
        const time3 = '16:00';
        const time4 = '19:30';

        if( (this.getTime(time) >= this.getTime(time1) && this.getTime(time) <= this.getTime(time2)) || 
            (this.getTime(time) >= this.getTime(time3) && this.getTime(time) <= this.getTime(time4))) {
            return false
        }
        return true;
    }

    private getTime(time: any) {
        return new Date(2019, 9, 2, time.substring(0, 2), time.substring(3, 5), 0, 0)
    }

}

const submitBtn = <HTMLButtonElement>document.getElementById('submitbtn');
const plateInputValue = <HTMLInputElement>document.getElementById('platenumber');
const dateInputValue = <HTMLInputElement>document.getElementById('date');
const timeInputValue = <HTMLInputElement>document.getElementById('time');
const retryLink = <HTMLInputElement>document.getElementById('retryLink');


let mainFunction = () => {
    // hide all elements when clicked again
    showHide(false, '.positive-result');
    showHide(false, '.negative-result');
    if(!validateInputs()){
        alert('Please all fields are required...');
        return;
    }
    validatePicoyPlaca(parseInt(plateInputValue.value), dateInputValue.value, timeInputValue.value);
}

submitBtn.addEventListener('click', mainFunction);
retryLink.addEventListener('click', () =>  window.location.reload(true));

// function to validate plateNumber
let validatePicoyPlaca = (plateNumber: number, date: string, time: string) => {
    let picoyPlacaObj = new PicoyPlaca(plateNumber, date, time);
    let canDrive = picoyPlacaObj.validatePlate();

    if(canDrive) {
        showHide(true, '.positive-result');
    } else {
        showHide(true, '.negative-result');
    }
}

let showHide = (show: boolean, elementToHideShow: string) => {
    let resultsContent = <HTMLElement>document.querySelector('.container__response__results');
    resultsContent.style.display = show ? 'flex' : 'none';
    let positiveResult = <HTMLElement>document.querySelector(elementToHideShow);
    positiveResult.style.display = show ? 'flex' : 'none';
}

let validateInputs = () => {
    if(plateInputValue.value !== '' && dateInputValue.value!=='' && timeInputValue.value!=='') {
        return true;
    }
    return false;
}





